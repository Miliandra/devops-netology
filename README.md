<h1>Course DevOps-Engineer from Netology</h1>
<h2>Student Karev Ilya</h2>

<ul>.gitignore terraform:
<li>1. **/.terraform/* - Исключение директории .terraform/* и все что имеется в ней, но игнорировать любые директории и фалы до нее </li>
<li>2.Исключение всех файлов с расширением *.tfstate и исключение всех файлов с маской *.tfstate.* </li>
<li>3.Исключение файла Crash.log </li>
<li>4.Исключение всех файлов с раширением *.tfvars </li>
<li>5.Исключение файла override.tf и override.tf.json, а также исключение любых файлов с маской *_override.tf и *_override.tf.json </li>
<li>6.!example_override.tf - Можно добавить опредленные фалы по данной маске, минуя исключение №5 </li>
<li>7.Исключение файлов по маске *tfplan* </li>
<li>8.Исключение файла .terraformrc и terraform.rc </li> 
<ul/>
